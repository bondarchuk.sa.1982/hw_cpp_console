#include <iostream>

using namespace std;

class Animal
{
public:
	Animal() {};
	~Animal() {};

	virtual void Voice() = 0;
};

class Dog : public Animal
{
public:
	Dog() {};
	~Dog() {};

	void Voice() override
	{
		cout << "Woof!" << endl;
	}
};

class Cat : public Animal
{
public:
	Cat() {};
	~Cat() {};

	void Voice() override
	{
		cout << "Meow!" << endl;
	}
};

class Duck : public Animal
{
public:
	Duck() {};
	~Duck() {};

	void Voice() override
	{
		cout << "Quack quack!" << endl;
	}
};

int main()
{
	Dog* dog   = new Dog();
	Cat* cat   = new Cat();
	Duck* duck = new Duck();

	Animal* animal[3] = { dog, cat, duck };

	for (const auto &obj : animal) {
		obj->Voice();
	}

    return 0;
}
